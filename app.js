var fs = require('fs');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var bp = require('body-parser');
var markov = require('./markov.js');
var port = 2600;

var file = 'glories.txt';
var wordcount = 0;
var depth = 2;
var markov = {
	// 'word pair': [
	//					['following', [relationships (',.!?')]],
	//					['following', [relationships (',.!?')]],
	//					['following', [relationships (',.!?')]]
	//				]
}


app.set('views', __dirname + '/views');
app.set('viev_engine', 'ejs');

app.use(bp.json());
app.use(bp.urlencoded());


app.get('/', function(req, res){
	res.sendfile('assets/index.html');
});

app.post('/', function(req, res){
	if(req.body.text) markov.parse(req.body.text);
	else res.send(400);
});

app.use(express.static(__dirname + '/assets'));

app.use(function(req, res){
	res.send(404);
});

app.listen(port);